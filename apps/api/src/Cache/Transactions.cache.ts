import { ITransactions } from "@ts/interfaces";

/**
 * @deprecated
 */
export const CacheTransactions = new Map<ITransactions["uid"], ITransactions>();