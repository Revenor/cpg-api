import { IImage } from "@ts/interfaces";

export const CacheImages = new Map<IImage["id"], IImage>();