import { IOrder } from "@ts/interfaces";

/**
 * @deprecated
 */
export const CacheOrder = new Map<IOrder["uid"], IOrder>();